const mongoose = require('mongoose');
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const uuid = require('uuid');
const moment = require('moment');

const UserSchema = new mongoose.Schema({
	firstname: {
		type: String,
		minlength: 3,
		maxlength: 255,
		required: true
	},
	lastname: {
		type: String,
		minlength: 3,
		maxlength: 255,
		required: true
	},
	email: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 255,
		unique: true
	},
	phone: {
		type: String,
		required: true,
		minlength: 11,
		maxlength: 13,
	},
	password: {
		type: String,
		required: true,
		minlength: 5,
		maxlength: 255,
	},
	api_token: {
		type: String,
		default: uuid.v4()
	},
	token_expiry: {
		type: Date,
		default: moment(new Date()).add(30, 'd').format('YYYY/MM/DD HH:mm:mm')
	},
	status: {
		type: String,
		default: 'active'
	},
	is_staff: {
		type: Boolean,
		default: false
	},
	is_super_admin: {
		type: Boolean,
		default: false
	},
	reg_date: {
		type: Date,
		default: moment(new Date()).format('YYYY/MM/DD HH:mm:mm')
	}
});

//custom method to generate authToken 
UserSchema.methods.signAuthToken = function () {
	const signedToken = jwt.sign({ api_token: this.api_token },
		config.get('jwt_private_key'));
	return signedToken;
}

//function to validate user 
function validateUser(user) {
	const schema = Joi.object({
		firstname: Joi.string().min(3).max(50).required(),
		lastname: Joi.string().min(3).max(50).required(),
		phone: Joi.string().min(11).max(13).required(),
		email: Joi.string().min(5).max(255).required().email(),
		password: Joi.string().min(5).max(255).required(),
		is_staff: Joi.boolean().required(),
		is_super_admin: Joi.boolean().required(),
		role: Joi.string()
	});

	return schema.validate(user);
}
const User = mongoose.model("User", UserSchema);

exports.validate = validateUser;
exports.User = User;
