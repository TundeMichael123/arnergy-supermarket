const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	description: {
		type: String
	},
	image_url: {
		type: String
	},
	price: {
		type: Number
	},
	category_id: {
		type: String
	}
});

const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;

