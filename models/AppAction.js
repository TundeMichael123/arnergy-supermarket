const mongoose = require('mongoose');

const AppActionSchema = new mongoose.Schema({
	action_name: {
		type: String,
		required: true
	},
	action_key: {
		type: String,
		required: true
	},
	paths: {
		type: Array
	},
	method: {
		type: String
	},
	description: {
		type: String
	}
}, { collection: "app_actions" });

const AppAction = mongoose.model("AppAction", AppActionSchema);
module.exports = AppAction;
