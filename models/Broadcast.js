const mongoose = require('mongoose');

const BroadcastSchema = new mongoose.Schema({
	message: {
		type: String,
		required: true
	},
	date_created: {
		type: Date
	},
	date_sent: {
		type: Date,
		default: Date.now
	},
	created_by: {
		type: String,
		required: true
	},
	sent_by: {
		type: String
	}
});

const Broadcast = mongoose.model("Broadcast", BroadcastSchema);
module.exports = Broadcast;