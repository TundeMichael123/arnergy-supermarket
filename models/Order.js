const mongoose = require('mongoose');
const moment = require('moment');

const OrderSchema = new mongoose.Schema({
	status: {
		type: String,
		default: 'new'
	},
	description: {
		type: String
	},
	customer_id: {
		type: String,
		required: true
	},
	date: {
		type: Date,
		default: moment(new Date()).format('YYYY/MM/DD HH:mm:mm')
	},
	total_qty: {
		type: Number
	},
	total_amount: {
		type: Number
	}
}, { collection: "orders" });

const Order = mongoose.model("Order", OrderSchema);
module.exports = Order;

