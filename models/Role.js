const mongoose = require('mongoose');

const RoleSchema = new mongoose.Schema({
	role: {
		type: String,
		required: true
	},
	description: {
		type: String
	}
}, { collection: "roles" });

const Role = mongoose.model("Role", RoleSchema);
module.exports = Role;
