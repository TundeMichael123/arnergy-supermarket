const mongoose = require('mongoose');

const RoleActionSchema = new mongoose.Schema({
	action_id: {
		type: String,
		required: true
	},
	role_id: {
		type: String,
		required: true
	}
}, { collection: "role_actions" });

const RoleAction = mongoose.model("RoleAction", RoleActionSchema);
module.exports = RoleAction;
