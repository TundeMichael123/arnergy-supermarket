const mongoose = require('mongoose');

const OrderLineSchema = new mongoose.Schema({
	order_id: {
		type: String,
		required: true
	},
	product_id: {
		type: String,
		required: true
	},
	qty: {
		type: Number
	},
	price: {
		type: Number
	}
}, { collection: "order_lines" });

const OrderLine = mongoose.model("OrderLine", OrderLineSchema);
module.exports = OrderLine;

