const mongoose = require('mongoose');

const UserRoleSchema = new mongoose.Schema({
	user_id: {
		type: String,
		required: true
	},
	role_id: {
		type: String,
        required: true
	}
}, { collection: "user_roles" });

const UserRole = mongoose.model("UserRole", UserRoleSchema);
module.exports = UserRole;
