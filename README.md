### An authentication API for a supermarket with the roles A, B & C.
** By design, the roles are dynamic which means more roles can be create and 
functionalities added or revoked from any role. **

A) SUPERVISOR 

1. Manages product categories 
2. Manages products 
3. Manages Employees 
4. Manages Clients 
5. Broadcasts Message 

B) EMPLOYEE 

1. Manage customer purchases
2. View products 
3. Manage his or her own sale
4. Post new sales

C) CLIENT 

1. View the list of his or her own purchases.
2. View the details of his or her own purchases.

---

## Getting started

To get the application started, follow the steps below:

1. Clone the repository 
2. Run **npm install** to install the packages.
3. To setup local mongo URI, open the config file at **config/config.env** and uncomment the local URI set. Please note that the **mongo.com** URI currently set in the config should work just fine except it may already contain some data.
4. Run **npm run seed** to seed the database. The will seed the database with the app actions 
and roles etc. Check **/seeder/** and **/data/** directories to see what is been seeded and 
how the seeders works. 
5. If seeding runs successfully, run **npm run dev** to start the http server in dev mode 
**npm start** to start in production mode.
6. Open the app on http://localhost:3000 or http://localhost:3000/health to view the API health check in JSON.


---

## Architecture

1. The project uses Nodejs, Express & MongoDB.
2. **models** folder contains the models.
3. **routes** folder contains the routes.
4. **public** contains and configured to serve the static files.
5. **data** contains sample json file for seeding the database.
6. **middleware** contains the security middlewares.
7. **config** contains the database and other configuration parameters.

## Security

1. The security is controlled by two middlewares. One for authentication and the other for 
authorization.
2. Roles are dynamic as well as the actions the members of the roles can perform.
3. The **/data/actions.json** contains the available access-limited routes in the project.
The file should be updated as more and more actions or features are added to the project.
4. The file **config/default.json** contains the **jwt** key used in signing users' API token.

## Basic usage

1. On **seeding** the application as described above, a default super admin user will be 
created. The credentials (email and password) of this user can be used to login **(http://localhost:3000/auth/login)** and get the api token for subsequent requests to the app.
2. To create a new user (supervisor), use the sample data below via Postman:

````
{
        "firstname": "Mr Supervisor",
        "lastname": "Mayor",
        "phone": "08012345678",
        "password": "mrsuper@123",
        "email": "mayor1@arnergyapp.com",
        "is_staff": true,
        "is_super_admin": false,
        "role": "Supervisor"
    }
````

3. On creation, (usually through a web UI), each role will be associated with actions the members can perform on the platform. The actions are made granular, for example, **view_products** grants access to the list of products while **delete_product** grants access to delete a product and it is possible for two or more roles to have the combinations of these access on different level.

4. The authorization middleware, **middleware/authorize** checks the path been requested against the paths mapped for the user's role before passing forward the request.

---