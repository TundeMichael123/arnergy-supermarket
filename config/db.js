const mongoose = require('mongoose');

const dbConnection = async () => {
    try {
        const conn = await mongoose.connect(process.env.MONGO_URI, {
            useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false
        });
        console.log(`Mongo DB connected: ${conn.connection.host}`);
    } catch (error) {
        console.log('Error connecting to Mongo');
        console.log(error);
        process.exit();
    }
}

module.exports = dbConnection;

//mongodb+srv://arnergy:y2656ydgxdVn5mZS@cluster0.evw8t.mongodb.net/arnergydb?
//retryWrites=true&w=majority
