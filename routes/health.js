const express = require('express');
const router = express.Router();

router.post('/', (req, res) => {
    return res.status(200).send({
        "status": "ok",
        "msg": "Application running smoothly!."
    });
});

router.get('/', (req, res) => {
    return res.status(200).send({
        "status": "ok",
        "msg": "Application running smoothly!."
    });
});


module.exports = router;