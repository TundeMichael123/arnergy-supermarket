const express = require('express');
const { User } = require('../models/User');
const router = express.Router();
const bcrypt = require("bcrypt");

router.post('/login', async (req, res) => {
    console.log(req.body);
    let user = await User.findOne({ email: req.body.email });
    if (!user) {
        return res.status(401).send({
            "status": "error",
            "msg": "Invalid username or password (1)."
        });
    }
    const pass = req.body.password
    bcrypt.compare(pass, user.password, function (err, matches) {
        if (matches) {
            const token = user.signAuthToken();
            return res.status(200).send({
                _id: user._id,
                firstname: user.firstname,
                lastname: user.lastname,
                phone: user.phone,
                email: user.email,
                is_staff: user.is_staff,
                api_token: token
            });
        }
        return res.status(401).send({
            "status": "error",
            "msg": "Invalid username or password (2)."
        });
    });

});

module.exports = router;