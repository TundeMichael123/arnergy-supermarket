const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send('<h3>Broadcasts</h3>');
});

router.post('/', (req, res) => {

});

router.post('/send/:id', (req, res) => {

});

router.put('/:id', (req, res) => {

});

router.delete('/:id', (req, res) => {

});

module.exports = router;