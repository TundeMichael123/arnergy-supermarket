const express = require('express');
const router = express.Router();
const authenticate = require("../middleware/authenticate");
const authorize = require("../middleware/authorize");

router.get('/', [authenticate, authorize], (req, res) => {
    res.send('<h3>Customers</h3>');
});

router.get('/:id', [authenticate, authorize], (req, res) => {
    res.send('<h3>Customers with ID:  ' + req.params.id + '</h3>');
});

router.post('/', [authenticate, authorize], (req, res) => {

});

router.put('/:id', [authenticate, authorize], (req, res) => {

});

router.delete('/:id', [authenticate, authorize], (req, res) => {

});

module.exports = router;