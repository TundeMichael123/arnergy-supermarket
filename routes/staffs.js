const express = require('express');
const router = express.Router();
const authenticate = require("../middleware/authenticate");
const authorize = require("../middleware/authorize");
const uuid = require('uuid');
const bcrypt = require("bcrypt");
const { User, validate } = require("../models/User");
const Role = require('../models/Role');
const UserRole = require('../models/UserRole');

/**
 * This route is for the staffs with SUPERVISOR role only.
 * The 2 middlewares ensures authentication and access control. 
 */

router.get('/', [authenticate, authorize], async (req, res) => {
    User.find({}, (err, users) => {
        var userMap = {};
        users.forEach((user) => {
            userMap[user._id] = user;
        });
        res.send(userMap);
    });
});

router.post('/', [authenticate, authorize], async (req, res) => {
    // Validate payload
    const { error } = validate(req.body);
    if (error) {
        //console.log(JSON.stringify(error)); 
        return res.status(400).send({ "status": "error", "msg": error.details[0].message });
    }
    //Check user already exists
    let user = await User.findOne({ email: req.body.email });
    if (user) {
        return res.status(400).send("User already registered.");
    }

    user = new User({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        phone: req.body.phone,
        password: req.body.password,
        email: req.body.email,
        is_staff: (req.body.is_staff && req.body.is_staff == true ? req.body.is_staff : false),
        api_token: uuid.v4()
    });
    user.password = await bcrypt.hash(user.password, 10);
    await user.save();
    let roleName = req.body.role;
    let role = await Role.findOne({ role: roleName });
    if (role) {
        await UserRole.create({ user_id: user._id, role_id: role._id });
    }
    const token = user.signAuthToken();
    res.header("x-auth-token", token).send({
        _id: user._id,
        firstname: user.firstname,
        lastname: user.lastname,
        phone: user.phone,
        email: user.email,
        is_staff: user.is_staff,
        api_token: token,
        role: role
    });
});

router.get('/:id', [authenticate, authorize], async (req, res) => {
    let user = await User.findOne({ _id: req.params.id });
    if (user) {
        return res.status(200).json({
            "status": "ok",
            "msg": "User detail fetched successfully.",
            "user": user
        });
    }
    return res.status(404).json({
        "status": "error",
        "msg": "Account not found"
    });
});

router.put('/:id', [authenticate, authorize], async (req, res) => {
    return res.status(200).json({ "status": "ok", "msg": "Update endpoint under construction!" });
});

router.delete('/:id', [authenticate, authorize], async (req, res) => {
    return res.status(200).json({ "status": "ok", "msg": "Delete endpoint under construction!" });
});

module.exports = router;