const AppActionSeeder = require('./seeders/AppActionSeeder');
const RoleSeeder = require('./seeders/RoleSeeder');
const SuperAdminSeeder = require('./seeders/SuperAdminSeeder');

const seed = async () => {
    // Run all seeders here
    AppActionSeeder();
    RoleSeeder();
    SuperAdminSeeder();
}

seed();
//process.exit();