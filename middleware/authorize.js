const { User } = require("../models/User");
const UserRole = require("../models/UserRole");
const AppAction = require("../models/AppAction");
const RoleAction = require("../models/RoleAction");

module.exports = async (req, res, next) => {

    if (!res.locals.api_token) {
        return res.status(401).send("Access denied. No token provided (2).");
    }
    try {
        //console.log(res.locals.api_token);
        const jwt = res.locals.api_token;
        let user = await User.findOne({ api_token: jwt.api_token });
        if (!user || user.status != 'active') { // Also check token expiry here
            return res.status(403).send("Access denied. Account may have been disabled or "
                + " expired security token. Please logout and login again.");
        }
        // Allow super admin all access.
        if (user.is_super_admin) {
            next();
        } else {
            let roles = await UserRole.find({ user_id: user._id });
            if (!roles || roles.length == 0) { // Also check token expiry here
                return res.status(403).send("Active account but you're yet to be assigned any "
                    + " role on the system. Please contact support.");
            }
            let roleIds = roles.map(r => r.role_id);
            let roleActions = await RoleAction.find({ 'role_id': { $in: roleIds } });
            if (!roleActions || roleActions.length == 0) {
                return res.status(403).send("Active account but you're yet to be assigned any "
                    + " active role on the system. Please contact support.");
            }
            let actionIds = roleActions.map(ra => ra.action_id);
            let actions = await AppAction.find({ '_id': { $in: actionIds } });
            if (!actions || actions.length == 0) {
                return res.status(403).send("Active account but you're yet to be assigned any "
                    + " active role on the system. Please contact  (2).");
            }
            const id = req.params.id;
            const path = req.baseUrl;
            let paths = actions.map(ac => ac.paths);
            let authorized = false;
            paths.forEach(pathList => {
                pathList.forEach(p => {
                    if (p === path) {
                        authorized = true;
                    }
                    if (id && (p === path + '/:id')) {
                        authorized = true;
                    }
                });
            });
            if (authorized) {
                console.log("******Authorized******")
                next();
            } else {
                return res.status(403).send("Active account but you're yet to be assigned any "
                    + " active role on the system. Please contact  (3).");
            }
        }
    } catch (ex) {
        console.log(ex);
        return res.status(401).send("Access denied. Security requirements not satisfied.");
    }
}