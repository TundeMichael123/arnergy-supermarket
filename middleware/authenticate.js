const jwt = require("jsonwebtoken");
const config = require("config");

module.exports = function (req, res, next) {
    //get the token from the header if present
    const token = req.headers["x-access-token"] || req.headers["authorization"];
    //if no token found, return response (without going to the next middelware)
    if (!token) {
        return res.status(401).send("Access denied. No token provided.");
    }
    try {
        //check the token and pass to req to the next middleware
        const tk = token.split("Bearer ").join("");
        //console.log(tk);
        const decoded = jwt.verify(tk, config.get("jwt_private_key"));
        if (!decoded) {
            return res.status(401).send("Access denied. Invalid token.");
        }
        //req.api_token = decoded;
        res.locals.api_token = decoded;
        next();
    } catch (ex) {
        //if invalid token
        //console.log("Key:  " + config.get("jwt_private_key"));
        console.log(ex);
        return res.status(401).send("Access denied. Invalid token (2).");
    }

}