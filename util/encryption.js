var bcrypt = require('bcrypt');

exports.encryptPassword = (password, callback) => {
    bcrypt.genSalt(10, (err, salt) => {
        if (err) {
            return callback(err);
        }
        bcrypt.hash(password, salt, (err, hash) => {
            return callback(err, hash);
        });
    });
};

exports.comparePassword = (plainPass, hashword, callback) => {
    bcrypt.compare(plainPass, hashword, function (err, isPasswordMatch) {
        return err == null ? callback(null, isPasswordMatch) : callback(err);
    });
};