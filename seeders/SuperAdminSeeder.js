const admins = require('../data/super_admin.json');
const dotenv = require('dotenv');
const dbConnection = require('../config/db');
const { User } = require("../models/User");
const bcrypt = require("bcrypt");
// Load config
dotenv.config({ path: "./config/config.env" });
// Connect to Mongo
dbConnection();

const SuperAdminSeeder = () => {

    admins.map(async (admin, index) => {
        let user = await User.findOne({ email: admin.email });
        if (!user) {
            admin.password = await bcrypt.hash(admin.password, 10);
            user = await User.create(admin);
            //console.log(user);
            //const token = user.signAuthToken();
            //console.log(token);
        } else {
            //console.log(user);
            //const token = user.signAuthToken();
            //console.log(token);
        }
        if (index === admins.length - 1) {
            console.log("*****SuperAdminSeeder:  Seeding Completed*****");
            //process.exit();
        }
    });
}

module.exports = SuperAdminSeeder;