const Role = require("../models/Role");
const roles = require('../data/roles.json');
const dotenv = require('dotenv');
const dbConnection = require('../config/db');
const AppAction = require("../models/AppAction");
const RoleAction = require("../models/RoleAction");
// Load config
dotenv.config({ path: "./config/config.env" });
// Connect to Mongo
dbConnection();

const RoleSeeder = () => {

    roles.map(async (r, index) => {
        let role = await Role.findOne({ role: r.role });
        if (!role) {
            role = await Role.create(r);
            const keys = r.action_keys;
            keys.forEach(async (key) => {
                let action = await AppAction.findOne({ action_key: key });
                if (action) {
                    await RoleAction.create(
                        { role_id: role._id, action_id: action._id }
                    );
                }
            });
        } else {
            //console.log(role);
        }
        if (index === roles.length - 1) {
            console.log("*****RoleSeeder:  Seeding Completed*****");
            //process.exit();
        }
    });
}

module.exports = RoleSeeder;