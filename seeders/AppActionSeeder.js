const AppAction = require("../models/AppAction");
const actions = require('../data/actions.json');
const dotenv = require('dotenv');
const dbConnection = require('../config/db');
// Load config
dotenv.config({ path: "./config/config.env" });
// Connect to Mongo
dbConnection();

const AppActionSeeder = () => {

    actions.map(async (a, index) => {
        let action = await AppAction.findOne({ action_key: a.action_key });
        if (!action) {
            action = await AppAction.create(a);
        } else {
            //console.log(action._id);
        }
        if(index === actions.length - 1){
            console.log("*****AppActionSeeder:  Seeding Completed*****");
            //process.exit();
        }
    });
}

module.exports = AppActionSeeder;