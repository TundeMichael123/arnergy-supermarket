const express = require('express');
const dotenv = require('dotenv');
const path = require('path');
const cors = require('cors');
const morgan = require('morgan');
const dbConnection = require('./config/db');

// Load config
dotenv.config({ path: "./config/config.env" });
// Connect to Mongo
dbConnection();

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// Configure static pages
app.use(express.static(path.join(__dirname, 'public')));
// Configure API routes
app.use('/auth', require('./routes/auth'));
app.use('/health', require('./routes/health'));
app.use('/staffs', require('./routes/staffs'));
app.use('/customers', require('./routes/customers'));
app.use('/products', require('./routes/products'));
app.use('/categories', require('./routes/categories'));
app.use('/broadcasts', require('./routes/broadcasts'));

// Configure dev logging
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`App started in ${process.env.NODE_ENV} mode on port ${PORT}`));

